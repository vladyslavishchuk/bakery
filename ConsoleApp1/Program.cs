﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (Bakery bakery = new Bakery())
            {
                Task.Run(bakery.Update);
                bool alive = true;
                while (alive)
                {
                    var key = Console.ReadKey();
                    switch (key.Key)
                    {
                        case ConsoleKey.B:
                            bakery.AddBaker(new Baker(bakery));
                            break;
                        case ConsoleKey.C:
                            bakery.AddClient(new Client());
                            break;
                        case ConsoleKey.Q:
                            alive = false;
                            break;
                        case ConsoleKey.UpArrow:
                            bakery.HungryTime += 100;
                            break;
                        case ConsoleKey.DownArrow:
                            bakery.HungryTime -= 100;
                            break;
                        case ConsoleKey.RightArrow:
                            bakery.BakingTime += 100;
                            break;
                        case ConsoleKey.LeftArrow:
                            bakery.BakingTime -= 100;
                            break;
                    }
                }
            }
        }
    }
    class Bakery : IDisposable
    {
        private int _HungryTime = 4000;
        public int HungryTime
        {
            get
            {
                return _HungryTime;
            }
            set
            {
                if (value > 0)
                    _HungryTime = value;
                else
                    _HungryTime = 0;
            }
        }
        private int _BakingTime = 2000;
        public int BakingTime
        {
            get
            {
                return _BakingTime;
            }
            set
            {
                if (value > 0)
                    _BakingTime = value;
                else
                    _BakingTime = 0;
            }
        }

        static Random random = new Random();

        bool alive = true;
        Queue<Bun> waiting = new Queue<Bun>();
        readonly object waitingMutex = new object();

        List<Client> clients = new List<Client>();
        readonly object clientsMutex = new object();

        List<Baker> bakers = new List<Baker>();
        readonly object bakersMutex = new object();

        public void AddClient(Client client)
        {
            lock (clientsMutex)
            {
                clients.Add(client);
                Task.Run(() => Take(client));
            }
        }

        public void AddBaker(Baker baker)
        {
            lock (bakersMutex)
            {
                bakers.Add(baker);
                Task.Run(() => Bake(baker));
            }
        }

        public async Task Update()
        {
            while (alive)
            {
                Console.Clear();
                Console.WriteLine("Baking time: {0,6}ms", BakingTime);
                Console.WriteLine("Hungry time: {0,6}ms", HungryTime);
                lock (waitingMutex)
                {
                    Console.WriteLine("Bakery: (Buns:{0})", waiting.Count);
                }

                lock (bakersMutex)
                {
                    Console.WriteLine("Bakers: ({0})", bakers.Count);
                    for (int i = 0; i < 10; i++)
                    {
                        if (i >= bakers.Count)
                        {
                            Console.WriteLine();
                            continue;
                        }
                        var b = bakers[i];
                        Console.WriteLine("ID: {0,2} Buns:  {1,7} Status:{2}", b.id, b.Buns, b.isBaking);
                    }
                }

                lock (clientsMutex)
                {
                    Console.WriteLine("Clients: ({0})", clients.Count);
                    for (int i = 0; i < 10; i++)
                    {
                        if (i >= clients.Count)
                        {
                            Console.WriteLine();
                            continue;
                        }
                        var b = clients[i];
                        Console.WriteLine("ID: {0,2} Needed: {1,7}", b.id, b.Needed);
                    }
                }

                await Task.Delay(200);
            }
        }

        async Task Take(Client client)
        {
            while (alive)
            {
                lock (waitingMutex)
                {
                    if (waiting.Count > 0)
                    {
                        client.Take(waiting.Dequeue());
                    }
                }
                if (client.Needed <= 0)
                {
                    lock (clientsMutex)
                    {
                        clients.Remove(client);
                    }
                    return;
                }
                await Task.Delay(random.Next(20) * 10 + HungryTime);
            }
        }

        async Task Bake(Baker baker)
        {
            while (alive)
            {
                var bun = await baker.Bake();
                lock (waitingMutex)
                {
                    waiting.Enqueue(bun);
                }
            }
        }

        public void Dispose()
        {
            alive = false;
        }

    }
    class Baker
    {
        static int last = 0;
        static Random r = new Random();

        public Bakery Work { get; private set; }
        public Baker(Bakery bakery)
        {
            Work = bakery;
        }

        public int id { get; } = ++last;
        public bool isBaking { get; private set; }
        public double Buns;
        public async Task<Bun> Bake()
        {
            isBaking = true;
            await Task.Delay(r.Next(20) * 10 + Work.BakingTime);
            Buns += 1;
            isBaking = false;
            return new Bun();
        }
    }
    class Client
    {
        static int last = 0;

        public int Needed { get; private set; }

        public int id { get; } = ++last;

        public Client()
        {
            Random r = new Random();
            Needed = r.Next(9) + 1;
        }

        public void Take(Bun bun)
        {
            Needed--;
        }
    }
    class Bun
    {

    }
}
